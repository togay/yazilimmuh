﻿using HavaDurumuBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace HavaDurumuBlog.Controllers
{
    public class IletisimController : Controller
    {
        // GET: Iletisim
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Email model)
        {
            MailMessage mailim = new MailMessage();
            mailim.To.Add("togaynuru94@gmail.com");
            mailim.From = new MailAddress("togaynuru94@gmail.com");
            mailim.Subject = "Blog Sitenizden Mesajınız Var." + model.Konu;
            mailim.Body ="Admin bey, " + model.AdSoyad + " kişisinden gelen mesaj içeriği aşağıdaki gibidir. <br>" + model.Icerik;
            mailim.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Credentials = new NetworkCredential("togaynuru94@gmail.com","T59648994a");
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;

            try
            {
                smtp.Send(mailim);
                TempData["Message"] = "Mesajınız iletilmiştir. En Kısa zamanda size geri dönüş sağlanacaktır";
            }
            catch (Exception ex)
            {
                TempData["Message"] = "Mesaj Gönderilemedi.Hata nedeni:" + ex.Message;
               
            }

            return View();
        }
    }
}