﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HavaDurumuBlog.Models
{
    public class Email
    {
        public string Konu { get; set; }

        public string Icerik { get; set; }

        public string AdSoyad { get; set; }

        public string Mail { get; set; }

    }
}