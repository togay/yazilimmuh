﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Bir derlemeyle ilgili Genel Bilgiler aşağıdaki öznitelik kümesi kullanılarak 
// denetlenir. Bir derlemeyle ilişkilendirilmiş bilgileri değiştirmek için bu
// öznitelik değerlerini değiştirin.
[assembly: AssemblyTitle("HavaDurumuBlog")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("HavaDurumuBlog")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// ComVisible özniteliğini false olarak ayarlarsanız COM bileşenleri bu 
// derlemedeki türleri göremez. Bu derlemedeki bir türe COM'dan erişmeniz 
// gerekiyorsa, o türde ComVisible özniteliğini true olarak ayarlayın.
[assembly: ComVisible(false)]

// Bu proje COM'a açılmışsa aşağıdaki GUID typelib'in ID'si içindir
[assembly: Guid("82f73cc3-1562-4db1-96a4-14e20db32b65")]

// Bir derlemenin sürüm bilgisi aşağıdaki dört değerden oluşur:
//
//      Ana Sürüm
//      Alt Sürüm 
//      Yapı Numarası
//      Düzeltme
//
// Tüm değerleri belirtebilirsiniz veya Düzeltme ve Yapı Numaralarına aşağıda 
// gösterildiği gibi '*' ile varsayılan değer atayabilirsiniz:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
